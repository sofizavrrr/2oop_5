﻿// При помощи цикла for изобразите на экране пирамиду из символов 'X'.
// Вся пирамида должна быть высотой 20 линий
#include "pch.h"
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	for (int i = 1; i < 21; i++)
	{
		cout<<setw(i)<<setfill('X')<<"X"<<endl;
	}
	system("pause");
	return 0;
}


